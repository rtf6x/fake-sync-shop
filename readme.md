# Fake Sync Shop API

### Prerequisites:
- Node.js
- npm

```
npm install
```

### Start the API (fake one, generates 1-10 customers once in 200ms):
```
npm start
```

### Start the Sync process (watch mode):
```
npm run sync
```

### Start the Sync process (full reindex mode):
```
npm run sync-reindex
```
