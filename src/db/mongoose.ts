import mongoose from 'mongoose';

mongoose.connect(process.env.DB_URI)
  .catch(err => {
      console.error('[mongoose] Error:', err);
    },
  );
mongoose.connection.once('open', () => {
  console.log('[mongoose] Connected...');
});

export default mongoose;
