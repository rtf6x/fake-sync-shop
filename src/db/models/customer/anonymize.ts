import { DocumentType } from '@typegoose/typegoose';

import { CustomerClass } from '../customer';

import { generateRandomString } from '../../../helpers/generateRandomString';
import { anonymizeEmail } from '../../../helpers/anonymizeEmail';

// Do not use the spread operator here because of the security reasons
export default function (customer: DocumentType<CustomerClass>) {
  return {
    _id: customer._id,
    firstName: generateRandomString(),
    lastName: generateRandomString(),
    email: anonymizeEmail(customer.email),
    address: {
      line1: generateRandomString(),
      line2: generateRandomString(),
      postcode: generateRandomString(),
      city: customer.address.city,
      state: customer.address.state,
      country: customer.address.country,
    },
    createdAt: customer.createdAt,
    updatedAt: customer.updatedAt,
  };
};
