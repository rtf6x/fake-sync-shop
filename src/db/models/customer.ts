import { Types } from 'mongoose';
import { DocumentType, getModelForClass, index, modelOptions, prop, Severity } from '@typegoose/typegoose';
import anonymize from './customer/anonymize';

export interface CustomerAddress {
  line1: string,
  line2: string,
  postcode: string,
  city: string,
  state: string,
  country: string,
}

// Better solution is to update all documents on save
// @post<CustomerClass>('save', (customer) => {
//   if (customer.firstName === 'Alex') {
//     console.log('Hi Alex!');
//   }
// })
@index({ updatedAt: 1 })
@modelOptions({
  options: {
    allowMixed: Severity.ALLOW,
  },
})
export class CustomerClass {
  @prop()
  public _id!: Types.ObjectId;

  @prop()
  public firstName!: string;

  @prop()
  public lastName!: string;

  @prop()
  public email!: string;

  @prop()
  public address: CustomerAddress;

  @prop()
  public createdAt: Date;

  @prop()
  public updatedAt: Date;

  public anonymize(this: DocumentType<CustomerClass>) {
    return anonymize(this);
  }
}

export const Customer = getModelForClass(CustomerClass, {
  schemaOptions: {
    timestamps: true,
    collection: 'customers',
  },
});
