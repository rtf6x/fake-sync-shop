import { Types } from 'mongoose';
import { getModelForClass, modelOptions, prop, Severity } from '@typegoose/typegoose';

export interface AnonymizedCustomerAddress {
  line1: string,
  line2: string,
  postcode: string,
  city: string,
  state: string,
  country: string,
}

@modelOptions({
  options: {
    allowMixed: Severity.ALLOW,
  },
})
export class AnonymizedCustomerClass {
  @prop()
  public _id!: Types.ObjectId;

  @prop()
  public firstName!: string;

  @prop()
  public lastName!: string;

  @prop()
  public email!: string;

  @prop()
  public address: AnonymizedCustomerAddress;

  @prop()
  public createdAt: Date;

  @prop()
  public updatedAt: Date;
}

export const AnonymizedCustomer = getModelForClass(AnonymizedCustomerClass, {
  schemaOptions: {
    timestamps: true,
    collection: 'customers_anonymised',
  },
});
