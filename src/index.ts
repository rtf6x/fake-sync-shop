import 'dotenv/config';
import { faker } from '@faker-js/faker';

import mongoose from './db/mongoose';
import { Customer } from './db/models/customer';

function createRandomCustomer() {
  return {
    _id: new mongoose.Types.ObjectId(),
    firstName: faker.person.firstName(),
    lastName: faker.person.lastName(),
    email: faker.internet.email(),
    address: {
      line1: faker.location.city(),
      line2: faker.location.city(),
      postcode: faker.location.zipCode(),
      city: faker.location.city(),
      state: faker.location.state({ abbreviated: true }),
      country: faker.location.countryCode(),
    },
  };
}

setInterval(async () => {
  if (mongoose.connection.readyState !== 1) {
    console.error(`[API] Mongoose is not connected yet (status: ${mongoose.connection.readyState}), waiting...`);
    return;
  }
  const count = Math.floor(Math.random() * 10 + 1);
  const customers = faker.helpers.multiple(createRandomCustomer, {
    count,
  });
  console.log(`[API] Inserting ${customers.length} customers`);
  await Customer.insertMany(customers);
}, 200);
