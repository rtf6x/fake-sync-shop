import fs from 'fs';
import 'dotenv/config';

import mongoose from './db/mongoose';
import { Customer } from './db/models/customer';
import { AnonymizedCustomer } from './db/models/anonymized-customer';

const UPDATES_DATE_FILENAME = 'sync.last-updated-time.txt';
const FULL_REINDEX_UPDATES_FILENAME = 'sync.full.last-updated-time.txt';

let fileName = UPDATES_DATE_FILENAME;
let fullReindex = false;
if (process.argv[process.argv.length - 1] === '--full-reindex') {
  console.log('[Sync] Running in FULL_REINDEX mode');
  fileName = FULL_REINDEX_UPDATES_FILENAME;
  fullReindex = true;
} else {
  console.log('[Sync] Running in SYNC mode');
}

let lastGetCustomersDate: string | number = fs.existsSync(fileName) ? parseInt(fs.readFileSync(fileName, 'utf8'), 10) : 0;
if (!lastGetCustomersDate && !fullReindex) {
  lastGetCustomersDate = Date.now();
}

setInterval(async () => {
  const start = performance.now();
  // Check if Mongo connection is up
  if (mongoose.connection.readyState !== 1) {
    console.error(`[Sync] Mongoose is not connected (status: ${mongoose.connection.readyState}), waiting...`);
    return;
  }
  // Get customers
  const customers = await Customer.find({ updatedAt: { $gt: lastGetCustomersDate } }).sort({ updatedAt: 1 }).limit(1000);

  if (!fullReindex && customers.length === 0) {
    return;
  }
  // [FR Mode] Final stage: no more customers, delete temporary file, exit
  if (fullReindex && customers.length === 0) {
    if (fs.existsSync(fileName)) fs.unlinkSync(fileName);
    console.log('No more customers to sync, exiting...');
    process.exit(0);
  }

  // Set update date for next iteration, sync customers, write file, log, next...
  lastGetCustomersDate = customers[customers.length - 1].updatedAt.getTime();
  // todo: Performance?... Didn't work with mongo for a long time. Maybe there's a method to update many at once
  await Promise.all(customers.map(c => {
    return AnonymizedCustomer.updateOne({ _id: c._id }, c.anonymize(), { upsert: true });
  }));
  await fs.writeFileSync(fileName, lastGetCustomersDate.toString());
  console.log(`[Sync] Synced ${customers.length} customers in ${Math.round(performance.now() - start)}ms`);
}, 1000);
