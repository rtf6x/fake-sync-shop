export function generateRandomString(length = 8) {
  const charset = 'abcdefghjkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ23456789';
  let result = '';
  for (let e = 0; e < length; e++) {
    const b = Math.floor(Math.random() * charset.length);
    result += charset.substring(b, b + 1);
  }
  return result;
}
