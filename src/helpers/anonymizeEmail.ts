import { generateRandomString } from './generateRandomString';

export function anonymizeEmail(email: string) {
  const result: string | string[] = email.split('@');
  result[0] = generateRandomString();
  return result.join('@');
}
